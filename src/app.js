const express = require('express')
const app = express()
const morgan = require('morgan')
const sequelize = require('./database')
const authJwt = require('./helpers/jwt')
const errorHandler = require('./helpers/error-handler')

require('dotenv/config')

const port = process.env.PORT || 3000
const api = process.env.API_URL || "/api/v1"

const gruposBotanicosRouter = require('./routes/grupos-botanicos')
const plantasRouter = require('./routes/plantas')
const produtosRouter = require('./routes/produtos')
const pedidosRouter = require('./routes/pedidos')
const usuariosRouter = require('./routes/usuarios')

const cors = require('cors')

app.use(cors())
app.options('*', cors())

//Middleware
app.use(express.json())
app.use(morgan('tiny'))
app.use(authJwt())
app.use(errorHandler)

//app.use(require("compression")());
//app.use(require("helmet")());
//app.use(require("./middlewares/api-headers"));
//app.use(require("./middlewares/validate-key"));

//app.use("/", require("./routes"));
//app.use("/api", require("./routes"));

//Routers
app.use(`${api}/grupos-botanicos`, gruposBotanicosRouter)
app.use(`${api}/plantas`, plantasRouter)
app.use(`${api}/produtos`, produtosRouter)
app.use(`${api}/pedidos`, pedidosRouter)
app.use(`${api}/usuarios`, usuariosRouter)

// TODO sequelize migrations
// TODO remove when migrating
sequelize.sync().then(() => {
    app.listen(port, () => {
        console.log('Server is up on port ' + port)
    })
})
