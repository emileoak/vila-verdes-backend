const express = require('express')
const GrupoBotanico = require('../models/grupo-botanico')
const Planta = require('../models/planta')
const router = express.Router()

router.get(`/`, async (req, res) => {
    try {
        const plantasLista = await Planta.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'grupoBotanicoId'],
            },
            include: [
                {
                    model: GrupoBotanico,
                    attributes: ['id', 'nome', 'descricao'],
                },
            ],
        })
        if (!plantasLista) {
            res.status(500).json({ success: false })
        }
        res.send(plantasLista)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.get(`/:id`, async (req, res) => {
    try {
        const planta = await Planta.findByPk(req.params.id, {
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'grupoBotanicoId'],
            },
            include: [
                {
                    model: GrupoBotanico,
                    attributes: ['id', 'nome', 'descricao'],
                },
            ],
        })
        if (!planta) {
            res.status(404).json({ message: 'Planta não encontrada.' })
        }
        res.status(200).send(planta)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.post(`/`, async (req, res) => {
    try {
        const grupoBotanico = await GrupoBotanico.findByPk(
            req.body.grupoBotanicoId
        )
        if (!grupoBotanico)
            return res.status(400).send('Grupo Botânico inválido')
        let planta = new Planta({
            nome: req.body.nome,
            descricao: req.body.descricao,
            especificacoes: req.body.descricao,
            imagens: req.body.imagens,
        })
        planta = await planta.save()
        await planta.setGrupoBotanico(grupoBotanico)
        if (!planta) return res.status(500).send('A planta não foi criada')
        res.send(planta)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.put('/:id', async (req, res) => {
    try {
        const grupoBotanico = await GrupoBotanico.findByPk(
            req.body.grupoBotanicoId
        )
        if (!grupoBotanico)
            return res.status(400).send('Grupo Botânico inválido')
        const updates = Object.keys(req.body)
        const planta = await Planta.findByPk(req.params.id)
        if (!planta) {
            return res.status(404).send()
        }
        updates.forEach((update) => (planta[update] = req.body[update]))
        await planta.save()
        res.send(planta)
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

router.delete('/:id', async (req, res) => {
    try {
        let planta = await Planta.destroy({
            where: { id: req.params.id },
        })
        if (planta) {
            return res.status(200).json({
                success: true,
                message: 'Planta removida com sucesso.',
            })
        } else {
            res.status(404).json({
                success: false,
                message: 'Planta não encontrada.',
            })
        }
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

module.exports = router
