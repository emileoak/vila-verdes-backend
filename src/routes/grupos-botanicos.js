const express = require('express')
const GrupoBotanico = require('../models/grupo-botanico')
const router = express.Router()

router.get(`/`, async (req, res) => {
    try {
        const gruposBotanicosLista = await GrupoBotanico.findAll()
        if (!gruposBotanicosLista) {
            res.status(500).json({ success: false })
        }
        res.status(200).send(gruposBotanicosLista)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.get(`/:id`, async (req, res) => {
    try {
        const grupoBotanico = await GrupoBotanico.findByPk(req.params.id)
        if (!grupoBotanico) {
            res.status(404).json({ message: 'Grupo botânico não encontrado.' })
        }
        res.status(200).send(grupoBotanico)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.post(`/`, async (req, res) => {
    try {
        let grupoBotanico = new GrupoBotanico({
            nome: req.body.nome,
            descricao: req.body.descricao,
        })
        grupoBotanico = await grupoBotanico.save()
        if (!grupoBotanico)
            return res.status(400).send('Grupo botânico não foi criado')
        res.send(grupoBotanico)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.put('/:id', async (req, res) => {
    try {
        const updates = Object.keys(req.body)
        const grupoBotanico = await GrupoBotanico.findByPk(req.params.id)
        if (!grupoBotanico) {
            return res.status(404).send()
        }
        updates.forEach((update) => (grupoBotanico[update] = req.body[update]))
        await grupoBotanico.save()
        res.send(grupoBotanico)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.delete('/:id', async (req, res) => {
    try {
        let grupoBotanico = await GrupoBotanico.destroy({
            where: { id: req.params.id },
        })
        if (grupoBotanico) {
            return res.status(200).json({
                success: true,
                message: 'Grupo botânico removido com sucesso.',
            })
        } else {
            res.status(404).json({
                success: false,
                message: 'Grupo botânico não encontrado.',
            })
        }
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

module.exports = router
