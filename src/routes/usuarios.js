const express = require('express')
const Usuario = require('../models/usuario')
const router = express.Router()

router.get(`/`, async (req, res) => {
    try {
        const usuarioLista = await Usuario.findAll()
        if (!usuarioLista) {
            res.status(500).json({ success: false })
        }
        res.send(usuarioLista)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.get(`/:id`, async (req, res) => {
    try {
        const usuario = await Usuario.findByPk(req.params.id)
        if (!usuario) {
            res.status(404).json({ message: 'Usuario não encontrado.' })
        }
        res.status(200).send(usuario)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.post(`/`, async (req, res) => {
    try {
        let usuario = new Usuario({
            nome: req.body.nome,
            sobrenome: req.body.sobrenome,
            email: req.body.email,
            senha: req.body.senha,
            endereco: req.body.endereco,
            celular: req.body.celular,
            cpf: req.body.cpf,
            tipo: req.body.tipo,
            active: req.body.active,
        })
        usuario = await usuario.save()
        if (!usuario) return res.status(500).send('O usário não foi criado')
        res.send(usuario)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.post(`/register`, async (req, res) => {
    try {
        let usuario = new Usuario({
            nome: req.body.nome,
            sobrenome: req.body.sobrenome,
            email: req.body.email,
            senha: req.body.senha,
            endereco: req.body.endereco,
            celular: req.body.celular,
            cpf: req.body.cpf,
            tipo: req.body.tipo,
            active: req.body.active,
        })
        usuario = await usuario.save()
        if (!usuario) return res.status(500).send('O usuário não foi criado')
        res.send(usuario)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.put('/:id', async (req, res) => {
    try {
        const updates = Object.keys(req.body)
        const usuario = await Usuario.findByPk(req.params.id)
        if (!usuario) {
            return res.status(404).send()
        }
        updates.forEach((update) => (usuario[update] = req.body[update]))
        await usuario.save()
        res.send(usuario)
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

router.post('/login', async (req, res) => {
    try {
        const usuario = await Usuario.findByCredentials(
            req.body.email,
            req.body.senha
        )
        const token = await usuario.generateAuthToken()
        res.send({ usuario, token })
    } catch (e) {
        res.status(400).send(e)
    }
})
router.delete('/:id', async (req, res) => {
    try {
        let usuario = await Usuario.destroy({
            where: { id: req.params.id },
        })
        if (usuario) {
            return res.status(200).json({
                success: true,
                message: 'Usuario removido com sucesso.',
            })
        } else {
            res.status(404).json({
                success: false,
                message: 'Usuario não encontrado.',
            })
        }
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

module.exports = router
