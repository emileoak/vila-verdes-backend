const express = require('express')
const Pedido = require('../models/pedido')
const router = express.Router()

router.get(`/`, async (req, res) => {
    const pedidosLista = await Pedido.findAll()
    if (!pedidosLista) {
        res.status(500).json({ success: false })
    }
    res.send(pedidosLista)
})

router.post(`/`, (req, res) => {
    const pedido = new Pedido({
        data: req.body.data,
        enderecoEntrega: req.body.enderecoEntrega,
        subTotal: req.body.subTotal,
        total: req.body.total,
        usuarioId: req.body.usuarioId,
        status: req.body.status
    })
    pedido
        .save()
        .then((createdPedido) => {
            res.status(201).json(createdPedido)
        })
        .catch((e) => {
            res.status(500).json({
                error: e,
                success: false,
            })
        })
})

module.exports = router
