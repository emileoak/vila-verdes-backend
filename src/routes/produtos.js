const express = require('express')
const Planta = require('../models/planta')
const Produto = require('../models/produto')
const router = express.Router()

//TODO Planta retornando com Plantum
router.get(`/`, async (req, res) => {
    try {
        let filter = {}
        if (req.query.plantas) {
            filter = { planta_id: req.query.plantas.split(',') }
        }
        const produtosLista = await Produto.findAll({
            attributes: { exclude: ['createdAt', 'updatedAt', 'plantaId'] },
            include: [{ model: Planta, attributes: ['id', 'nome', 'imagens'] }],
            where: filter,
        })
        if (!produtosLista) {
            res.status(500).json({ success: false })
        }
        res.send(produtosLista)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.get(`/:id`, async (req, res) => {
    try {
        const produto = await Produto.findByPk(req.params.id, {
            attributes: { exclude: ['createdAt', 'updatedAt', 'plantaId'] },
            include: [{ model: Planta, attributes: ['id', 'nome'] }],
        })
        if (!produto) {
            res.status(404).json({ message: 'Produto não encontrado.' })
        }
        res.status(200).send(produto)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.post(`/`, async (req, res) => {
    try {
        let produto = new Produto({
            nome: req.body.nome,
            descricao: req.body.descricao,
            valor: req.body.valor,
            desconto: req.body.desconto,
            active: req.body.active,
            planta_id: req.body.plantaId,
        })
        produto = await produto.save()
        if (!produto) return res.status(500).send('O produto não foi criado')
        res.send(produto)
    } catch (e) {
        return res.status(500).json({ success: false, error: e })
    }
})

router.put('/:id', async (req, res) => {
    try {
        const updates = Object.keys(req.body)
        const produto = await Produto.findByPk(req.params.id)
        if (!produto) {
            return res.status(404).send()
        }
        updates.forEach((update) => (produto[update] = req.body[update]))
        await produto.save()
        res.send(produto)
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

router.delete('/:id', async (req, res) => {
    try {
        let produto = await Produto.destroy({
            where: { id: req.params.id },
        })
        if (produto) {
            return res.status(200).json({
                success: true,
                message: 'Produto removido com sucesso.',
            })
        } else {
            res.status(404).json({
                success: false,
                message: 'Produto não encontrado.',
            })
        }
    } catch (e) {
        return res.status(400).json({ success: false, error: e })
    }
})

module.exports = router
