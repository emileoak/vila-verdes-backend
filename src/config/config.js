const { Sequelize } = require('sequelize');

const sequelize = new Sequelize({
    host: process.env.DB_HOST || 'localhost',
    dialect: 'postgres',
    port: process.env.DB_PORT || 5432,
    database: process.env.DB_NAME || 'vila_verdes_dev',
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    timezone: process.env.DB_TIMEZONE || 'Etc/UTC',
    pool: {
        max: parseInt(process.env.DB_MAX_CONNECTIONS) || 100,
        min: parseInt(process.env.DB_MIN_CONNECTIONS) || 1,
        idle: parseInt(process.env.DB_IDLE_TIME) || 60000,
        acquire: parseInt(process.env.DB_AQCUIRE_TIME) || 10000,
        evict: parseInt(process.env.DB_EVICT_TIME) || 10000
    },
    define: { timestamps: true, underscored: true }
})

module.exports = sequelize