const { DataTypes } = require('sequelize')
const sequelize = require('../config/config')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const Usuario = sequelize.define(
    'Usuario',
    {
        nome: { type: DataTypes.TEXT, allowNull: false },
        sobrenome: { type: DataTypes.TEXT },
        email: {
            type: DataTypes.TEXT,
            isEmail: true,
            allowNull: false,
            unique: true,
        },
        senha: {
            type: DataTypes.STRING(64),
            is: /^[0-9a-f]{64}$/i,
            allowNull: false,
        },
        endereco: { type: DataTypes.STRING },
        celular: { type: DataTypes.STRING },
        cpf: { type: DataTypes.STRING },
        googleId: { type: DataTypes.STRING },
        facebookId: { type: DataTypes.STRING },
        tipo: { type: DataTypes.INTEGER, defaultValue: 1 },
        active: { type: DataTypes.BOOLEAN, defaultValue: true },
    },
    {
        tableName: 'usuarios',
    }
)

Usuario.prototype.toJSON = function () {
    const usuario = this
    const { senha, ...usuarioObject } = { ...usuario.dataValues }
    return usuarioObject
}

Usuario.addHook('beforeSave', async (usuario) => {
    if (usuario.changed('senha') && usuario.senha) {
        usuario.senha = await bcrypt.hash(usuario.senha, 8)
    }
})

Usuario.findByCredentials = async (email, senha) => {
    if (!email || !senha) {
        throw { error: 'email e senha não podem ser nulos' }
    }
    const usuario = await Usuario.findOne({ where: { email } })
    if (!usuario) {
        throw { error: 'Falha no login' }
    }
    const isMatch = await bcrypt.compare(senha, usuario.senha)
    if (!isMatch) {
        throw { error: 'Falha no login' }
    }
    return usuario
}

Usuario.prototype.generateAuthToken = async function () {
    const usuario = this
    const token = jwt.sign(
        { id: usuario.id, tipo: usuario.tipo },
        process.env.AUTHORIZATION_KEY || 'authKey',
        { expiresIn: '1w' }
    )
    usuario.createToken({ token })
    return token
}

module.exports = Usuario
