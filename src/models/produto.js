const { DataTypes } = require('sequelize')
const sequelize = require('../config/config')

const Produto = sequelize.define(
    'Produto',
    {
        nome: { type: DataTypes.TEXT, allowNull: false, unique: true },
        descricao: { type: DataTypes.TEXT },
        valor: { type: DataTypes.FLOAT, allowNull: false, defaultValue: 0 },
        desconto: { type: DataTypes.FLOAT },
        quantidade: { type: DataTypes.INTEGER },
        active: { type: DataTypes.BOOLEAN, defaultValue: true },
    },
    {
        tableName: 'produtos',
    }
)

module.exports = Produto
