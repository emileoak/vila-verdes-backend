const {DataTypes} = require('sequelize');
const sequelize = require('../config/config');

const ProdutoPedido = sequelize.define('produtos_pedidos', {
    quantidade: DataTypes.INTEGER
});

module.exports = ProdutoPedido