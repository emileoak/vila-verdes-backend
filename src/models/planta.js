const { DataTypes } = require('sequelize')
const sequelize = require('../config/config')

const Planta = sequelize.define(
    'Planta',
    {
        nome: { type: DataTypes.TEXT, allowNull: false, unique: true },
        descricao: { type: DataTypes.TEXT },
        especificacoes: { type: DataTypes.TEXT },
        imagens: { type: DataTypes.ARRAY(DataTypes.TEXT) },
    },
    {
        tableName: 'plantas',
        freezeTableName: true
    }
)

module.exports = Planta
