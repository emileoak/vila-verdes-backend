const { DataTypes } = require('sequelize')
const sequelize = require('../config/config')

const Pedido = sequelize.define(
    'Pedido',
    {
        data: { type: DataTypes.DATE },
        enderecoEntrega: { type: DataTypes.TEXT },
        subTotal: { type: DataTypes.FLOAT },
        total: { type: DataTypes.FLOAT },
        status: { type: DataTypes.INTEGER, defaultValue: 1 },
    },
    {
        tableName: 'pedidos',
    }
)

module.exports = Pedido
