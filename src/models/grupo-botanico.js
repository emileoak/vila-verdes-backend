const { DataTypes } = require('sequelize')
const sequelize = require('../config/config')

const GrupoBotanico = sequelize.define(
    'grupoBotanico',
    {
        nome: { type: DataTypes.TEXT, allowNull: false, unique: true },
        descricao: { type: DataTypes.TEXT },
    },
    {
        tableName: 'grupos_botanicos',
    }
)

module.exports = GrupoBotanico
