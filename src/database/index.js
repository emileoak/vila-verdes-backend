const sequelize = require('../config/config')

const GrupoBotanico = require('../models/grupo-botanico')
const Planta = require('../models/planta')
const Pedido = require('../models/pedido')
const Produto = require('../models/produto')
const ProdutoPedido = require('../models/produto_pedido')
const Usuario = require('../models/usuario')
const Token = require('../models/token')

// function notNull() { return { foreignKey: { allowNull: false } }; }

GrupoBotanico.hasMany(Planta)
Planta.belongsTo(GrupoBotanico)

Planta.hasMany(Produto, {
    foreignKey: { name: 'planta_id', field: 'planta_id', allowNull: true },
})
Produto.belongsTo(Planta, {
    foreignKey: { name: 'planta_id', field: 'planta_id', allowNull: true },
})

Usuario.hasMany(Token, { foreignKey: { allowNull: false } })
Token.belongsTo(Usuario)

Pedido.belongsToMany(Produto, { through: ProdutoPedido })
Produto.belongsToMany(Pedido, { through: ProdutoPedido })

// User.hasMany(Token, notNull());
// Token.belongsTo(User);

// User.hasMany(Extreme_Mode_Match, notNull());
// Extreme_Mode_Match.belongsTo(User);

// User.hasMany(Horde_Mode_Match, notNull());
// Horde_Mode_Match.belongsTo(User);

module.exports = sequelize
