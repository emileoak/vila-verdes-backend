const expressJwt = require('express-jwt')

function authJwt() {
    const secret = process.env.SECRET || "authKey"
    const api = process.env.API_URL || "/api/v1"
    return expressJwt({
        secret,
        algorithms: ['HS256'],
        isRevoked: isRevoked,
    }).unless({
        path: [
            { url: /\/api\/v1\/produtos(.*)/, methods: ['GET', 'OPTIONS'] },
            {
                url: /\/api\/v1\/grupos-botanicos(.*)/,
                methods: ['GET', 'OPTIONS'],
            },
            { url: /\/api\/v1\/plantas(.*)/, methods: ['GET', 'OPTIONS'] },
            `${api}/usuarios/login`,
            `${api}/usuarios/register`,
        ],
    })
}

async function isRevoked(req, payload, done) {
    if (payload.tipo !== 2) {
        done(null, true)
    }
    done()
}

module.exports = authJwt
