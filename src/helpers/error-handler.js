function errorHandler(err, req, res, next) {
    if (err && err.name === 'UnauthorizedError') {
        // Erro JWT
        return res.status(401).json({ message: 'Usuário não autorizado' })
    }
    if (err && err.name === 'ValidationError') {
        // Erro de validação
        return res.status(401).json({ message: err })
    }
    // Erro geral
    return res.status(500).json(err)
}


module.exports = errorHandler;